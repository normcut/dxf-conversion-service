# Dxf Conversion Service
This is a slim express wrapper providing a basic file conversion between svg and dxf files.  It utilizes the [dxf npm library]() as well as a combination of inkscape and pstoedit to do the actual conversion. 

## How to run
### Development
Make sure you have `inkscape` and `pstoedit` installed.
Then just run `npm run start`. This should start the development server on port 3000.
### Production
A simple `docker-compose up --build` should should (re)build the docker image and run the container. Environment and Port can be configured using `NODE_ENV` and `NODE_PORT` env variables.
