FROM ubuntu:20.04

# Create app directory
WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install node
RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_15.x  | bash -
RUN apt-get -y install nodejs

RUN npm ci --only=production

# Add inkscape Repo
RUN apt-get update && apt-get install -y \
	software-properties-common
RUN add-apt-repository ppa:inkscape.dev/stable
RUN apt-get -y --allow-unauthenticated update

# Install inkscape & pstoedit
RUN apt-get install -y --allow-unauthenticated inkscape
RUN apt-get install -y --allow-unauthenticated pstoedit

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "npm", "run", "start" ]
