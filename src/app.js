const express = require('express')
const multer = require('multer')
const upload = multer({dest: 'uploads/'})
const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');


const Helper = require('dxf').Helper

const app = express()
const port = process.env.NODE_PORT || 3000

app.post('/dxf2svg', upload.single('dxfFile'), async function (req, res, next) {
    var uploadedFile = req.file;
    if (uploadedFile === undefined) {
        res.status(400).send('No file.');
        return;
    }

    await fs.readFile(uploadedFile.path, "utf8", async function (err, data) {
        const dxfHelper = new Helper(data);

        const originalName = uploadedFile.originalname;
        let pos = originalName.lastIndexOf(".");
        svgFileName = originalName.substr(0, pos < 0 ? originalName : pos) + ".svg";

        const svgString = dxfHelper.toSVG()
        const svgData = new Uint8Array(Buffer.from(svgString));
        const svgPath = path.join(uploadedFile.destination, svgFileName)

        await fs.writeFile(svgPath, svgData,  async function (err) {
            if (err) throw err;
            res.download(svgPath, async (err)  => {
                if (err) {
                    throw err
                } else {
                    console.log("Processed file successfully.")
                    // Download was successful, delete files on server
                    await fs.unlink(uploadedFile.path, () => {});
                    await fs.unlink(svgPath, () => {});
                }
            });
        });
    });
})

app.post('/svg2dxf', upload.single('svgFile'), async function (req, res, next) {
    var uploadedFile = req.file
    if (uploadedFile === undefined) {
        res.status(400).send('No file.');
        return;
    }

    const originalName = uploadedFile.originalname;
    let pos = originalName.lastIndexOf(".");
    let dxfFileName = originalName.substr(0, pos < 0 ? originalName : pos) + ".dxf";
    let epsFileName = originalName.substr(0, pos < 0 ? originalName : pos) + ".eps";
    const dxfPath = path.join(uploadedFile.destination, dxfFileName);
    const epsPath = path.join(uploadedFile.destination, epsFileName);



    exec(`inkscape --export-ps-level=3 --export-type=eps --export-filename=${epsPath} ${uploadedFile.path}`, (err, stdout, stderr) => {
        if (err) {
            throw err;
        }
        console.log("Successfully converted svg to temporary eps.")
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);

        exec(`pstoedit -dt -f 'dxf:-polyaslines -mm' "${epsPath}" "${dxfPath}"`, (err, stdout, stderr) => {
            if (err) {
                throw err;
            }
            console.log("Successfully converted eps to dxf.")
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);

            res.download(dxfPath, async (err)  => {
                if (err) {
                    throw err
                } else {
                    console.log("Processed file successfully.")
                    await fs.unlink(uploadedFile.path, () => {});
                    await fs.unlink(epsPath, () => {});
                    await fs.unlink(dxfPath, () => {});
                }
            });
        });
    });
})

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})
